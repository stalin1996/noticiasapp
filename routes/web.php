<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// AL PONER LA / EN LA DIRECCION RAIZ DEL PROYECTO REDIRECCIONARA LA VISTA DE WELCOME
Route::get('/', function () {
    return view('inicio');
});

//SE CREO UN RESOURCE PARA EL  MANEJO DE LOS LOGIN
Route::resource('login','loginController');


// SE CREA UN RESOURCE PARA QUE ACCEDA A LOS METODOS DE USUARIOCONTROLLER Y ESTE CONTROLADOR SE CREÓ PARA LA GESTION DE USUARIOS
Route::resource('usuarios','usuarioController');

/*
METODOS QUE TIENE POR DEFECTO UN CONTROLLER C
//index,show,create,store,edit,update,delete
//mostrar todo, mostrar indivisual,crear,guardar,eliminar

Route::get('/', 'loginController@index');



//RUTA QUE SE USA PARA OBTENER UNA CLAVE ENCRIPTADA 
Route::get('crear_password/{clave}',function($clave){
	return bcrypt($clave);
});
*/


