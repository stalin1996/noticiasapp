<!-- EXTENDEMOS DE LA VISTA PADRE		-->
@extends('layouts.padre')
@section('contenido')

<!-- DECIMOS QUE SECTION REMPLAZAR CON EL CONTENIDO QUE ESTA HASTA EL STOP		-->
<h1 align="center">GESTION DE USUARIOS</h1>
<div class="row">
	<div class="col-lg-10">
		<!--CREAMOS LA TABLA DE USUARIOS-->
		<table class="table" width="80%">
			<thead>
				<tr>
					<th>ID</th>
					<th>NOMBRE DE USUARIO</th>
					<th>EDITAR</th>
				</tr>
			</thead>
			<tbody>
			<!--RECORREMOS LA COLLECCION DE USUARIOS-->
			@foreach ($usuarios as $item)
				<tr>
					<td>{{ $item->id }}</td>
					<td>{{ $item->username }}</td>
					<td> <a class="btn btn-success" href="usuarios/{{ $item->id }}/edit ">EDITAR</a></td>
				</tr>
			@endforeach				
			</tbody>
		</table>
	</div>		
</div>



@stop

