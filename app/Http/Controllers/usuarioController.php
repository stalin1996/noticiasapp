<?php

namespace App\Http\Controllers;

use App\usuario2Model;
use Illuminate\Http\Request;



//SE USA PARA PODER RETORNAR VISTA CON LARAVEL 
use View;

class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    //PARA CREAR UN CONTROLADOR EN LARAVEL SE EJECUTA EL COMANDO php artisan make:controller nombreCOntroller --resource
    public function index()
    {
        //CREO UNA VARIABLE QUE CONTIENE TODO LOS REGISTROS DE USUARIOS EN LA BD , EN LA TABLA DE USUARIOS (POR MEDIO DEL MODELO USUARIO2MODEL)
        $usuarios=usuario2Model::all(['id','username']);

        //RETORNAMOS VISTA Y PASAMOS COMO PARAMETRO LA COLLECCION
        return View('admin.usuarios.index',compact('usuarios'));
        
        /*
        foreach ($usuarios as $item) {
            echo 'el usuario '.$item->id.' el nombre de usuario es '.$item->username;
        }
        */
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function show(usuarioModel $usuarioModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario=usuario2Model::find($id);
        return View('admin.usuarios.editar',compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuarioModel $usuarioModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuarioModel  $usuarioModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(usuarioModel $usuarioModel)
    {
        //
    }

}
